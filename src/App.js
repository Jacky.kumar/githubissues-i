import React, { Component } from "react";

import "./App.css";
import Issues from "./components/Issues";
import issuesData from "./issuesData";
import Header from "./components/Header";


import Fuse from "fuse.js";





class App extends Component {


state = {
    result: issuesData.map(issuesData => (
      <Issues>{issuesData}</Issues>
    )),
    
   
  };
  
Labels = () => {
  return issuesData.reduce((labels, issues) => {
    issues.labels.forEach(label => {
      if (!labels.includes(label.name)) {
        labels.push(label.name);
      }
    });
    return labels;
  }, []);
};

Authors = () => {
  return issuesData.reduce((Author, issues) => {
    if (!Author.includes(issues.user.login)) {
      Author.push(issues.user.login);
    }
    return Author;
  }, []);
};

countOpenState=()=>{
  let count=0;
  this.state.result.forEach((issues)=>{
    if(issues.props.children.state==='open')
    count++;
  })
  return count;
}

countClosedState=()=>{
  let count=0;
  this.state.result.forEach((issues)=>{
    if(issues.props.children.state==='close')
    count++;
  })
  return count;
}

handleState = clickState => {
  console.log(this.state)
    this.setState({
      result: this.state.result
        .filter(issuesData => {
          if (issuesData.props.children.state === clickState) return issuesData;
        })
        
    });
  };
  handleLabel = event => {
    this.setState({
      result: issuesData
        .filter(issues => {
          let avaliable = false;
          issues.labels.forEach(label => {
            if (label.name === event.target.value) {
              avaliable = true;
            }
          });
          if (avaliable) return issues;
        })
        .map(issuesData => <Issues>{issuesData}</Issues>)
    });
  };
  handleAuthor = event => {
    this.setState({
      result: issuesData
        .filter(issues => issues.user.login === event.target.value)
        .map(issuesData => <Issues >{issuesData}</Issues>)
    });
  };
  handleTitleSearch = event => {
    var options = {
      shouldSort: true,

      maxPatternLength: 10,
      minMatchCharLength: 3,
      keys: ["title"]
    };
    var fuse = new Fuse(issuesData, options); // "list" is the item array
    var titleMatched = fuse.search(event.target.value);
    this.setState({
      result: titleMatched.map(issuesData => (
        <Issues>{issuesData}</Issues>
      ))
    });
  };

  handleSort = event => {
    let lengthOfIssues = issuesData.length;
    if (event.target.value === "newest")
      this.setState({
        result: issuesData.filter(issues=>issues).sort(
          (issuesData1, issuesData2) =>
           ( new Date(issuesData2.created_at).getTime() -
            new Date(issuesData1.created_at).getTime())
        ).map(issuesData => (
          <Issues id={issuesData.id}>{issuesData}</Issues>
        ))
      });
    if (event.target.value === "oldest")
      this.setState({
        result: issuesData.filter(issues=>issues).sort(
          (issuesData1, issuesData2) =>
           ( new Date(issuesData1.created_at).getTime() -
            new Date(issuesData2.created_at).getTime())
        ).map(issuesData => (
          <Issues id={issuesData.id}>{issuesData}</Issues>
        ))
      });
    if (event.target.value === "recently updated")
      this.setState({
        result: issuesData.filter(issues=>issues)
          .sort(
            (issuesData1, issuesData2) =>
              new Date(issuesData2.updated_at).getTime() -
              new Date(issuesData1.updated_at).getTime()
          )
          .map(issuesData => (
            <Issues id={issuesData.id}>{issuesData}</Issues>
          ))
      });
    if (event.target.value === "least recently updated")
      this.setState({
        result: issuesData.filter(issues=>issues)
          .sort(
            (issuesData1, issuesData2) =>
              new Date(issuesData1.updated_at).getTime() -
              new Date(issuesData2.updated_at).getTime()
          )
          .map(issuesData => <Issues id={issuesData.id}>{issuesData}</Issues>)
      });
  };

  render() {
    return (
      <div>
        <Header
          labelData={this.Labels}
          AuthorData={this.Authors}
          onClick={this.handleState}
          onChangeLabel={this.handleLabel}
          onChangeAuthor={this.handleAuthor}
          onSort={this.handleSort}
          openIssues={this.countOpenState}
          closeIssues={this.countClosedState}
          onClickTitle={this.handleTitleSearch}
        />
        <div>{this.state.result}</div>
      </div>
    );
  }
}

export default App;
