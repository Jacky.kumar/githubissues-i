import React from 'react';
import './Header.css';
import Button from '@material-ui/core/Button';

const Header=(props)=>{
 

  return(
    <div className="Header-items">
    <Button onClick={()=>props.onClick('open')}>
     {props.openIssues()} open
    </Button>
    <Button onClick={()=>props.onClick('close')}>
    {props.closeIssues()} closed
    </Button>

   <label> Search By Label<select onChange={(e)=>props.onChangeLabel(e)}>{props.labelData().map(label=><option>{label}</option>)}</select></label> 
   <label>Search By Author<select onChange={(e)=>props.onChangeAuthor(e)}>{props.AuthorData().map(Author=><option>{Author}</option>)}</select></label> 
  <label>sort by
    <select onClick={(e)=>props.onSort(e)} >
      <option>newest</option>
      <option>oldest</option>
      <option>recently updated</option>
      <option>least recently updated</option>
    </select>
    </label>
    <label onChange={(e)=>props.onClickTitle(e)} >Search By Title<input type="text"></input></label>
    

    </div>
  )
}
export default Header;