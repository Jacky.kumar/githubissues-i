import React from 'react';
import Button from '@material-ui/core/Button';
import './Label.css'
const Label=(props)=>{
  let buttonColor=`#${props.children.color}`
  return(

  <label  ><Button className="label-button" variant="contained" style={{backgroundColor:buttonColor,marginLeft:10 } }>{props.children.name}</Button></label>
    
  )
}
 
export default Label;