import React from "react";
import Label from "./Label";
import "./Issues.css";
var moment = require("moment");

const Issues = props => {
  let id = `#${props.children.id}`;
  let time = moment(props.children.created_at).fromNow();
  return (
    <div className="Issues-div">
      <div ><i class="material-icons">error_outline
      </i>
        {props.children.title}
        {props.children.labels.map(labels => (
          <Label>{labels}</Label>
        ))}
      </div>
      <div className="issues-time">
        {id} opened {time} by {props.children.user.login}
      </div>
    </div>
  );
};
export default Issues;
